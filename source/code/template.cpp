#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>

#define sz(a) ((int)((a).size()))
#define make_unique(x)            \
  sort((x).begin(), (x).end()); \
  (x).erase(unique((x).begin(), (x).end()), (x).end())

using namespace std;
using namespace __gnu_pbds;
// mt19937 rnd(239);
mt19937 rnd(chrono::steady_clock::now().time_since_epoch().count());


template <typename T>
using ordered_set =
        tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;

typedef long long ll;
typedef long double ld;

void solve() {
  
}

int32_t main() {
  ios::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);
  int t = 1;
  cin >> t;
  while (t--) {
    solve();
#ifdef ONPC
    cout << "__________________________" << endl;
#endif
    }
#ifdef ONPC
    cerr << endl
         << "finished in " << clock() * 1.0 / CLOCKS_PER_SEC << " sec" << endl;
#endif
}
