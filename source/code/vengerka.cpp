/* Венгерский алгоритм.
 * Даниил Швед, 2008. danshved [no-spam] gmail.com
 * Реализация навеяна псевдокодом А.С.Лопатина из книги
 * "Оптимизация на графах (алгоритмы и реализация)".
 */
#include <vector>
#include <limits>
using namespace std;

typedef pair<int, int> PInt;
typedef vector<int> VInt;
typedef vector<VInt> VVInt;
typedef vector<PInt> VPInt;

const int inf = numeric_limits<int>::max();

/*
 * Решает задачу о назначениях Венгерским методом.
 * matrix: прямоугольная матрица из целых чисел (не обязательно положительных).
 *         Высота матрицы должна быть не больше ширины.
 * Возвращает: Список выбранных элементов, по одному из каждой строки матрицы.
 */
VPInt hungarian(const VVInt &matrix) {
   
   // Размеры матрицы
   int height = matrix.size(), width = matrix[0].size();
   
   // Значения, вычитаемые из строк (u) и столбцов (v)
   VInt u(height, 0), v(width, 0);
   
   // Индекс помеченной клетки в каждом столбце
   VInt markIndices(width, -1);
   
   // Будем добавлять строки матрицы одну за другой
   for(int i = 0; i < height; i++) {
      VInt links(width, -1);
      VInt mins(width, inf);
      VInt visited(width, 0);
      
      // Разрешение коллизий (создание "чередующейся цепочки" из нулевых элементов)
      int markedI = i, markedJ = -1, j;
      while(markedI != -1) {
         // Обновим информацию о минимумах в посещенных строках непосещенных столбцов
         // Заодно поместим в j индекс непосещенного столбца с самым маленьким из них
         j = -1;
         for(int j1 = 0; j1 < width; j1++)
            if(!visited[j1]) {
               if(matrix[markedI][j1] - u[markedI] - v[j1] < mins[j1]) {
                  mins[j1] = matrix[markedI][j1] - u[markedI] - v[j1];
                  links[j1] = markedJ;
               }
               if(j==-1 || mins[j1] < mins[j])
                  j = j1;
            }
            
         // Теперь нас интересует элемент с индексами (markIndices[links[j]], j)
         // Произведем манипуляции со строками и столбцами так, чтобы он обнулился
         int delta = mins[j];
         for(int j1 = 0; j1 < width; j1++)
            if(visited[j1]) {
               u[markIndices[j1]] += delta;
               v[j1] -= delta;
            } else {
               mins[j1] -= delta;
            }
         u[i] += delta;
         
         // Если коллизия не разрешена - перейдем к следующей итерации
         visited[j] = 1;
         markedJ = j;
         markedI = markIndices[j];   
      }
      
      // Пройдем по найденной чередующейся цепочке клеток, снимем отметки с
      // отмеченных клеток и поставим отметки на неотмеченные
      for(; links[j] != -1; j = links[j])
         markIndices[j] = markIndices[links[j]];
      markIndices[j] = i;
   }
   
   // Вернем результат в естественной форме
   VPInt result;
   for(int j = 0; j < width; j++)
      if(markIndices[j] != -1)
         result.push_back(PInt(markIndices[j], j));
   return result;
}

///////////////////////////////////////////potokiNRNU MEPhI – Useless but powerfull Page 4 of 5
}
Features Benefits Templates Plans Pricing Help Register Log In
Join Project
Cannot grant access; cont
7 Misc
7.1 Allocator
const int MAX_MEM = 1e8;
int mpos = 0;
char mem[MAX_MEM];
inline void * operator new ( size_t n ) {
char *res = mem + mpos;
mpos += n;
//assert(mpos <= MAX_MEM);
return (void *)res;
}
inline void operator delete ( void * ) { }
7.2 Vengerka
/* .
* , 2008. danshved [no-spam] gmail.com
* ..
* " ( )".
*/
#include <vector>
#include <limits>
using namespace std;
typedef pair<int, int> PInt;
typedef vector<int> VInt;
typedef vector<VInt> VVInt;
typedef vector<PInt> VPInt;
const int inf = numeric_limits<int>::max();
/*
* .
* matrix: ( ).
* .
* : , .
*/
VPInt hungarian(const VVInt &matrix) {
//
int height = matrix.size(), width = matrix[0].size();
// , (u) (v)
VInt u(height, 0), v(width, 0);
//
VInt markIndices(width, -1);
//
for(int i = 0; i < height; i++) {
VInt links(width, -1);
VInt mins(width, inf);
VInt visited(width, 0);
// ( " " )
int markedI = i, markedJ = -1, j;
while(markedI != -1) {
//
// j
j = -1;
for(int j1 = 0; j1 < width; j1++)
if(!visited[j1]) {
if(matrix[markedI][j1] - u[markedI] - v[j1] <
mins[j1]) {
mins[j1] = matrix[markedI][j1] - u[markedI]
- v[j1];
links[j1] = markedJ;
}
if(j==-1 || mins[j1] < mins[j])
j = j1;
}
// (markIndices[links[j]], j)
// ,
int delta = mins[j];
for(int j1 = 0; j1 < width; j1++)
if(visited[j1]) {
u[markIndices[j1]] += delta;
v[j1] -= delta;
} else {
mins[j1] -= delta;
}
u[i] += delta;
// -
visited[j] = 1;
markedJ = j;
markedI = markIndices[j];
}
// ,
//
for(; links[j] != -1; j = links[j])
markIndices[j] = markIndices[links[j]];
markIndices[j] = i;
}
//
VPInt result;
for(int j = 0; j < width; j++)
if(markIndices[j] != -1)
result.push_back(PInt(markIndices[j], j));
return result;
}
///////////////////////////////////////////potoki
[Tinkoff Generation 2020-2021. A. #1]: 19
[a-202

#include <bits/stdc++.h>

using namespace std;

const int INF = 1e9 + 7;

int get(char x) {
    if ('a' <= x && x <= 'z')
        return x - 'a';
    return x - 'A' + 26;
}

vector<int> F, C, COST, st, fin, p, d, num[106];

void add(int a, int b, int c, int d) {
    num[a].push_back(F.size());
    F.push_back(0);
    C.push_back(c);
    COST.push_back(d);
    st.push_back(a);
    fin.push_back(b);
}

void add_reb(int v, int to, int cost) {
    add(v, to, 1, cost);
    add(to, v, 0, -cost);
}

int main() {
    cin.tie(nullptr);
    cout.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int n, k;
    cin >> n >> k;
    string a, b;
    cin >> a >> b;
    vector<vector<int>> gogo(k, vector<int> (k));
    for (int i = 0; i < n; ++i) {
        --gogo[get(a[i])][get(b[i])];
    }
    F.reserve(k * (k + 1) * 2);
    C.reserve(k * (k + 1) * 2);
    st.reserve(k * (k + 1) * 2);
    fin.reserve(k * (k + 1) * 2);
    for (int i = 0; i < k; ++i) {
        add_reb(k + k, i, 0);
        for (int j = 0; j < k; ++j)
            add_reb(i, j + k, gogo[i][j]);
        add_reb(i + k, k + k + 1, 0);
    }
    p.resize(k + k + 2);
    while (true) {
        d.assign(k + k + 2, INF);
        d[k + k] = 0;
        set<pair<int, int>> otr;
        otr.insert({0, k + k});
        while (otr.size()) {
            int v = (*otr.begin()).second;
            otr.erase(otr.begin());
            for (auto i : num[v])
                if (C[i] - F[i] && d[st[i]] != INF && d[st[i]] + COST[i] < d[fin[i]]) {
                    d[fin[i]] = d[st[i]] + COST[i];
                    p[fin[i]] = i;
                    otr.insert({0, fin[i]});
                }
        }
        if (d[k + k + 1] == INF)
            break;
        vector<int> a;
        int v = k + k + 1;
        while (v != k + k) {
            a.push_back(p[v]);
            v = st[a.back()];
        }
        for (auto x : a) {
            ++F[x];
            --F[x ^ 1];
        }
    }
    vector<int> ans(k);
    int answ = 0;
    for (int i = 0; i < fin.size(); ++i)
        if (st[i] < k + k && fin[i] < k + k && F[i] > 0)
            ans[st[i]] = fin[i] - k;
    for (int i = 0; i < k; ++i)
        answ -= gogo[i][ans[i]];
    cout << answ << '\n';
    for (int i = 0; i < k; ++i)
        if (ans[i] < 26)
            cout << char('a' + ans[i]);
        else
            cout << char('A' + ans[i] - 26);
    cout << '\n';
    return 0;

