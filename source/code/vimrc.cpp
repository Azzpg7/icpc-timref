set background=dark
colorscheme iceberg
set t_vb=
set noerrorbells
set visualbell
set nocompatible
set encoding=utf-8
set fileencodings=utf-8
set termencoding=utf-8
set mousehide
set mouse=a
set number
set relativenumber
set hlsearch
set incsearch
syntax enable
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set expandtab
set autoindent
set smartindent
set scrolloff=7
set sidescrolloff=7
"zR - open all, zM - close all
set nofoldenable
set foldmethod=syntax
set foldnestmax=1
set foldlevel=2
set autochdir
set nobackup
set noswapfile
set t_Co=256 
" Keybinds
let mapleader = ","
" Better call Netrw
nnoremap <Leader>s :Lexplore<CR>
" Compile & Run
inoremap <F5> <Esc> :w <CR> :term g++ -g -std=c++17 -Wall -Wextra -Wshadow -DONPC -O2 %:p -o %:p:r && %:p:r <CR>
nnoremap <F5> <Esc> :w <CR> :term g++ -g -std=c++17 -Wall -Wextra -Wshadow -DONPC -O2 %:p -o %:p:r && %:p:r <CR>
" Clear search higlighting
nnoremap <silent> <Esc><Esc> :nohlsearch<CR><Esc>  
" Auto complete (, {, [
inoremap {<CR> {<CR>}<Esc>O
inoremap (<CR> (<CR>)<Esc>O
inoremap [<CR> [<CR>]<Esc>O
" Copy entire buffer
nnoremap <Leader>y :%y+<CR>
" Netrw settings
let g:netrw_banner = 0  " hide banner to show use I
let g:netrw_winsize = 20  " configure size
let g:netrw_liststyle = 3