namespace fft {

typedef complex<double> cd;
typedef vector<cd> vcd;

vcd fft(const vcd &a) {
  int n = sz(a);
  int nl = 0;
  while ((1 << nl) < n) ++nl;

  vector<int> rev(n);
  rev[0] = 0;
  int maxpow = -1;
  for (int i = 1; i < n; ++i) {
    if ((i & (i - 1)) == 0) ++maxpow;
    rev[i] = rev[i ^ (1 << maxpow)];
    rev[i] |= (1 << (nl - maxpow - 1));
  }

  vcd roots(n);
  for (int i = 0; i < n; ++i) {
    double alpha = 2 * m_pi * i / n;
    roots[i] = cd(cos(alpha), sin(alpha));
  }

  vcd cur(n);
  for (int i = 0; i < n; ++i) cur[i] = a[rev[i]];

  for (int len = 1; len < n; len <<= 1) {
    int root_step = n / (len << 1);
    for (int to = 0; to < n; to += len) {
      for (int i = 0; i < len; ++i) {
        cd u = cur[to];
        cd v = cur[to + len] * roots[i * root_step];
        cur[to] = u + v;
        cur[to + len] = u - v;
        ++to;
      }
    }
  }

  return cur;
}

vcd fft_rev(const vcd &a) {
  vcd res = fft(a);
  for (int i = 0; i < sz(res); ++i) res[i] /= sz(a);
  reverse(res.begin() + 1, res.end());
  return res;
}

vector<int> fft_mult(const vector<int> &_a, const vector<int> &_b) {
  int n = sz(_a);
  int m = sz(_b);
  vcd a(n), b(m);
  for (int i = 0; i < n; ++i) a[i] = cd(_a[i], 0);
  for (int i = 0; i < m; ++i) b[i] = cd(_b[i], 0);

  ll max2 = 1;
  while (max2 < n + m) max2 <<= 1;
  a.resize(max2, 0);
  b.resize(max2, 0);

  vcd a_res = fft(a);
  vcd b_res = fft(b);

  vcd c_res(max2);
  for (int i = 0; i < max2; ++i) c_res[i] = a_res[i] * b_res[i];
  vcd c = move(fft_rev(c_res));

  while (sz(c) && (int)round(c.back().real()) == 0) c.pop_back();

  vector<int> res;
  for (int i = 0; i < sz(c); ++i) res.push_back((int)round(c[i].real()));

  return res;
}

}  // namespace fft
